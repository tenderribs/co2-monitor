#include <Arduino.h>
#include <Button2.h>
#include <SPI.h>
#include <SoftwareSerial.h>
#include <TFT_eSPI.h>

#include "MHZ19.h"
#define BAUDRATE \
    9600  // Device to MH-Z19 Serial baudrate (should not be changed)

#define BTN1 35
#define BTN2 0

unsigned long timer = 0;
int level = 0;
int8_t temp = 0;

const int beeper = 2;
const int pwmChannel = 2;

const int ylwLED = 13;
const int redLED = 15;

const int rx = 21;
const int tx = 22;

byte bl = LOW;

SoftwareSerial uart(rx, tx);
MHZ19 co2;
TFT_eSPI tft = TFT_eSPI();  // Invoke custom library
Button2 btn1(BTN1);
Button2 btn2(BTN2);

/**
 * let out a short beep with a piezo buzzer on channel 2
 */
void beep() {
    for (size_t i = 0; i < 2; i++) {
        ledcWriteTone(pwmChannel, 1400);
        delay(100);
        ledcWrite(pwmChannel, 0);  // write duty cycle 0 to turn off
        delay(50);
    }
}

void setGPIO() {
    ledcAttachPin(beeper, pwmChannel);
    ledcSetup(pwmChannel, 8000, 12);

    pinMode(TFT_BL, OUTPUT);
    pinMode(ylwLED, OUTPUT);
    pinMode(redLED, OUTPUT);
}

/**
 * Short little intro jingle
 */
void intro() {
    digitalWrite(redLED, HIGH);
    digitalWrite(ylwLED, HIGH);
    beep();
    digitalWrite(redLED, LOW);
    digitalWrite(ylwLED, LOW);
}

/** Code duplication but who cares. Both buttons can be used to toggle screen on
 */
void setButtonHandlers() {
    btn1.setPressedHandler([](Button2& b) {
        bl = !bl;
        digitalWrite(TFT_BL, bl);
    });

    btn2.setPressedHandler([](Button2& b) {
        bl = !bl;
        digitalWrite(TFT_BL, bl);
    });
}

void setup() {
    uart.begin(BAUDRATE);

    setGPIO();
    intro();
    setButtonHandlers();

    tft.init();

    co2.begin(uart);
    co2.autoCalibration();
}

void output(const int& level, const int& temp) {
    tft.fillScreen(TFT_BLACK);
    tft.setCursor(5, 40);
    tft.setTextColor(TFT_WHITE);
    tft.setTextFont(4);
    tft.setRotation(3);

    tft.print(level);
    tft.println(" ppm");
    tft.setCursor(5, 80);
    tft.print(temp);
    tft.println(" C");
}

void evaluate() {
    digitalWrite(redLED, LOW);
    digitalWrite(ylwLED, LOW);

    // Some random source on threshold levels for CO2
    // https://www.kane.co.uk/knowledge-centre/what-are-safe-levels-of-co-and-co2-in-rooms
    if (level >= 1250) {
        digitalWrite(ylwLED, HIGH);
    }
    if (level >= 1600) {
        digitalWrite(redLED, HIGH);
        beep();
    }
}

void loop() {
    if (millis() - timer >= 20 * 1000) {
        level = co2.getCO2();
        temp = co2.getTemperature();

        output(level, temp);
        evaluate();

        timer = millis();
    }

    btn1.loop();
    btn2.loop();
}