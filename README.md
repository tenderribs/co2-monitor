# Co2 Monitor

Ever forget to open your window, only to find yourself in nasty, stuffy a few hours later? This little weekend project is fun to assemble and remedies this problem by beeping and flashing at you if the air gets bad.

|                       Board                        |          Case           |
| :------------------------------------------------: | :---------------------: |
| ![Assembled thingy](./img/IMG_20211226_203413.jpg) | ![Case](./img/case.png) |

## Doing it yourself

### Materials:

- [Piezo Beeper](https://www.bastelgarage.ch/piezo-buzzer-summer-aktiv)
- [Display](https://www.bastelgarage.ch/128x128-1-44inch-rgb-tft-lcd-display-spi)
- [CO2 Sensor](https://www.bastelgarage.ch/mh-z19b-ndir-co2-sensor)
- A TTGO T-Display ESP32 microcontroller from Aliexpress
- 50mm x 70mm Protoboard
- 4 x 2.5mm hex-screw
- Some resistors and LEDs

|                 Schematics                  |                  Dimensions                  |
| :-----------------------------------------: | :------------------------------------------: |
| ![Schematic](./img/IMG_20211226_202013.jpg) | ![Dimensions](./img/IMG_20211226_202002.jpg) |

### Software

In the project folder run

```sh
intercept-build make all
```
to build and upload the compiled and linked sketch to the board. This project is based on platformio, so make sure this is installed.

## Case

There is a small case to print out if you like. The STL files as well as the freecad design files are located in /case.